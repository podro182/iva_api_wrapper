Overview
~~~~~~~~

A wrapper that makes it easy to use Ingenuity Variant Analysis (IVA) through its API. The tool filters and annotates vcf
files through IVA. A custom analysis should already be in place on the Ingenuity Variant Analysis web interface. Using
the IVA API it takes around 6-7 minutes to process a single exome. Keep in mind that the API does not offer the option
to delete samples from your IVA account, so manual cleanup is necessary!

Install
~~~~~~~

To clone the script: :code:`git clone git@gitlab-rh.computerome.dk:genomic-medicine/iva_api_wrapper.git`

The tool cannot stand alone yet, but in Computerome it is sufficient to load anaconda and it runs smoothly: :code:`module load anaconda3/4.4.0`


Quickstart
~~~~~~~~~~

::
    
    #!/usr/bin/env bash
     
    git clone git@gitlab-rh.computerome.dk:genomic-medicine/iva_api_wrapper.git
    cd iva_api_wrapper
    
    iva_email='jane.doe@email.com'
    analysis_name='your_analysis'
    outdir='absolute/dir/to/exported/vcf'
    
    echo "user = $iva_email
    analysis = $analysis_name
    output_dir = $outdir
    client_id = 34d550e810c9cd37ef08c50151b10373
    client_secret = 44443de653e828f227432d989e5db31f
    " > .conf
     
    mkdir -p $outdir
    python3 iva_api/iva_api.py /home/projects/cu_10047/pipelines/research/gatk3_GRCh38/mutect2/Fase1000_tumor_matched_m2_snvs_indels.vcf


Run
~~~

To see all command line options you can do: :code:`python3 iva_api.py -h`

**iva_api.py** - usage example:

This runs the IVA analysis named common_filter on all variant call files in a directory:

:code:`python3 iva_api.py /path/to/*vcf* -u jane.doe@email.com -a your_analysis -o <output_directory>
--client_id <client_id> --client_secret <client_secret>`

Configure
~~~~~~~~~

An :code:`/path/to/iva_api_wrapper/.conf` is being used to set any command line options (except the vcf files) by using lines
like: :code:`<command line option (without --)> = <value>`.

In case of overlaps command line options override the ones set by the configuration file. In case of multiple
users, each user can specify its own configuration file by specifying command line option :code:`-c`. The above example
usage would look like:

:code:`python3 iva_api.py /path/to/*vcf* -c /path/to/my-config`

where :code:`my-config` contains::

	user = jane.doe@email.com
	analysis = <your_analysis>
	output_dir = <output_directory>
	client_id = <client_id>
	client_secret = <client_secret>

