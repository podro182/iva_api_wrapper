import pytest
import os
import subprocess
import re
import time
import configargparse
import gzip
import string
import random
import sys
import json


def vcf_file(file):
    msg = file + ' does not seem to be a vcf file! Check the file or the extension (.vcf or vcf.gz)'
    if file.endswith('.vcf.gz'):
        with gzip.open(file, 'rt') as f:
            start_file = f.readline()
    elif file.endswith('.vcf'):
        with open(file) as f:
            start_file = f.readline()
    else:
        raise configargparse.ArgumentTypeError(msg)
    if not start_file.startswith('##fileformat=VCFv'):
        raise configargparse.ArgumentTypeError(msg)
    return file


def email(mail):
    if not re.match("[^@]+@[^@]+\.[^@]+", mail):
        raise configargparse.ArgumentTypeError(mail + ' is not a valid email address.')
    return mail


def test_email():
    assert 's.k@r.d' == email('s.k@r.d')
    with pytest.raises(configargparse.ArgumentTypeError):
        email('s.kATrDOTd')
    with pytest.raises(configargparse.ArgumentTypeError):
        email('s.k@.d')
    with pytest.raises(configargparse.ArgumentTypeError):
        email('s.k@rd')


def writeable_directory(directory):
    if not os.path.isdir(directory):
        raise configargparse.ArgumentTypeError(directory + ' is not a directory')
    if not os.access(directory, os.W_OK):
        raise configargparse.ArgumentTypeError(directory + ' is not a writeable directory')
    return directory


def test_writeable_directory():
    with pytest.raises(configargparse.ArgumentTypeError):
        writeable_directory('ndkjsabksa')
    assert writeable_directory('.') == '.'


def get_error_message(response):
    errors = json.loads(response).get('errors')
    error_list = []
    if errors:
        for error in errors:
            error_message = error.get('details').get('error.message') \
                if error.get('details').get('error.message') else ''
            error_list.append(' '.join([error.get('message'), error_message]).rstrip(' '))
    return '\n'.join(error_list)


def test_get_error_message():
    response = \
        '{"method":"partner","errors":[{"code":"isatab.invalid.referenced.file.name","details":{"fieldname":"Raw Data' \
        ' File","filename":"a_project.txt","error.message":"error_message1","value":"variants_small.vcf"},"message":"' \
        'message1"},{"code":"isatab.invalid.referenced.file.name","details":{"fieldname":"Raw Data File","filename":"' \
        'a_project.txt","value":"variants_small.vcf"},"message":"message2"}]}'
    assert get_error_message(response) == "message1 error_message1\nmessage2"

    response = '{"method":"partner"}'
    assert get_error_message(response) == ""


def get_iva_status(response, export):
    if export:
        status = 'DONE' if response else 'FAILED'
    else:
        status = json.loads(response).get('status')
    return status


def test_get_iva_status():
    response = '{"title":"Title","analysis-name":"variants_small","status":"DONE","stage":"Successfully completed"}'
    assert get_iva_status(response, False) == 'DONE'
    assert get_iva_status(response, True) == 'DONE'
    assert get_iva_status('', True) == 'FAILED'


def get_token():
    token = \
        subprocess.Popen("""curl -sS -d "grant_type=client_credentials&client_id=""" + args.client_id +
                         """&client_secret=""" + args.client_secret +
                         """" https://api.ingenuity.com/datastream/api/v1/oauth/access_token | """
                         """sed -e 's/"//g' -e 's/{//g' -e 's/}//g' | cut -d ':' -f 2""",
                         shell=True, stdout=subprocess.PIPE).communicate()[0].rstrip(b'\n')
    return token.decode('utf-8')


def get_http_status_and_output(response):
    headers, body = response.rsplit('\r\n\r\n', 1)
    http_status = [ele.split(' ')[1] for ele in headers.split('\r\n') if ele.startswith('HTTP')]
    http_status = [int(ele) for ele in http_status]
    http_status = max(http_status)
    return http_status, body


def test_get_http_status_and_output():
    response = 'HTTP/1.1 100 Continue \r\n\r\nHTTP/1.1 200 \r\nConnection: close\r\n\r\n##fileformat'
    assert get_http_status_and_output(response) == (200, '##fileformat')
    response = \
        'HTTP/1.1 404 \r\nContent-Length: 0\r\nDate: Thu, 19 Apr 2018 12:11:11 GMT\r\nConnection: close\r\n\r\n'
    assert get_http_status_and_output(response) == (404, '')


def upload_vcf(path):
    token = get_token()
    upload_message = subprocess.Popen(
        """curl -i -X POST -H "Content-Type: multipart/form-data" -H "Authorization: """ + token +
        """" --form file=@""" + os.path.join(path, 'project.zip') +
        """ https://api.ingenuity.com/datastream/api/v1/datapackages/""",
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    return upload_message.decode('utf-8')


def check_analysis_iva_status(datapackage_url):
    token = get_token()
    analysis_status = subprocess.Popen(
        """curl -i -H "Content-Type: application/json" -H "Authorization: Bearer """ + token +
        """" -X GET """ + datapackage_url,
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    return analysis_status.decode('utf-8')


def export_vcf(datapackage_url):
    token = get_token()
    export_message = subprocess.Popen(
        """curl -i -H "Content-Type: application/json" -H "Authorization: Bearer """ + token +
        """" -X GET """ + re.sub('datapackages', 'export', datapackage_url),
        shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
    return export_message.decode('utf-8')


def make_request(fun, var, vcf, export=False):
    count = 1
    response = fun(var)
    http_status, body = get_http_status_and_output(response)
    while http_status >= 400 and count < 5:
        time.sleep(60)
        response = fun(var)
        http_status, body = get_http_status_and_output(response)
        count += 1
    if http_status >= 400:
        print('\nProcessing of ' + os.path.basename(vcf) + ' stopped. The HTTP requests give status code ' +
              str(http_status) + '. ' + json.loads(body).get('error'))
        iva_status = None
    else:
        iva_status = get_iva_status(body, export)
        if iva_status == 'FAILED':
            print('\nERROR: ' + get_error_message(body))
    return http_status, iva_status, body


# def test_make_request():
#     message = '{"method":"partner integration","creator":"savvas.kinalis@regionh.dk","users":["savvas.kinalis@regionh' \
#               '.dk"],"title":"Title","analysis-name":"variants_small","status":"DONE","stage":"Pipeline successfully ' \
#               'completed","results-url":"https://api.ingenuity.com/datastream/analysisStatus.jsp?packageId=DP_1048537' \
#               '600","status-url":"https://api.ingenuity.com/v1/datapackages/DP_1048537600","pipeline-name":"Variant A' \
#               'nalysis Custom Pipeline","percentage-complete":100,"results-ids":["9863599"],"results-redirect-url":"h' \
#               'ttps://variants.ingenuity.com/va/?a=9863599","export-url":"https://api.ingenuity.com/v1/export/DP_1048' \
#               '537600","warnings":[{"code":"isatab.recommended.field","details":{"fieldname":"Investigation File Name' \
#               '","filename":"r_project.txt","fieldtype":"field"},"message":"r_project.txt: The field Investigation Fi' \
#               'le Name is missing. We strongly recommend including this field to improve the relevance of the analysi' \
#               's.  This field may be required in the future."}]}'
#     pytest_mock.mocker.patch('get_iva_status', message=message, return_value='DONE')


def write_files_for_upload(tmp_path, vcf, user, analysis):
    with open(os.path.join(tmp_path, 'a_project.txt'), 'w') as a_f:
        a_f.write('Sample Name\tRaw Data File\n' +
                  os.path.basename(vcf).rstrip('.gz').rstrip('.vcf') + '\t' + os.path.basename(vcf))
    with open(os.path.join(tmp_path, 's_project.txt'), 'w') as s_f:
        s_f.write('Sample Name\n' +
                  os.path.basename(vcf).rstrip('.gz').rstrip('.vcf'))
    with open(os.path.join(tmp_path, 'r_project.txt'), 'w') as r_f:
        r_f.write('Analysis Title\t' + os.path.basename(vcf).rstrip('.gz').rstrip('.vcf') + '\n'
                  'Analysis Pipeline Name\tVariant Analysis Custom Pipeline/' + user + '/' + analysis + '\n'
                  'Processor Email\t' + user + '\n'
                  'Case Sample Names\t' + os.path.basename(vcf).rstrip('.gz').rstrip('.vcf'))
    with open(os.path.join(tmp_path, 'i_project.txt'), 'w') as i_f:
        i_f.write('STUDY\n'
                  'Study Identifier\t' + os.path.basename(vcf).rstrip('.gz').rstrip('.vcf') + '\n' +
                  'Study Title\tTitle\n'
                  'Study File Name\ts_project.txt\n'
                  'STUDY ASSAYS\n'
                  'Study Assay Technology Platform\tIllumina\n'
                  'Study Assay Technology Type\tNGS\n'
                  'Study Assay File Name\ta_project.txt\n' +
                  'STUDY CONTACTS\n'
                  'Study Person Roles\tinvestigator\n'
                  'Study Person Affiliation\tRigshospitalet\n'
                  'Study Person Last Name\t' + user.split('@').pop(0).split('.').pop().capitalize() + '\n'
                  'Study Person First Name\t' + user.split('@').pop(0).split('.').pop(0).capitalize() + '\n'
                  'Study Person Mid Initials\n'
                  'Study Person Email\t' + user
                  )
    _ = subprocess.Popen(
        ['zip', '-j9', os.path.join(tmp_path, 'project.zip'), vcf, os.path.join(tmp_path, 'a_project.txt'),
         os.path.join(tmp_path, 's_project.txt'), os.path.join(tmp_path, 'r_project.txt'),
         os.path.join(tmp_path, 'i_project.txt')], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    return


if __name__ == "__main__":

    script_path = os.path.dirname(os.path.realpath(__file__))
    parser = configargparse.ArgParser(formatter_class=configargparse.ArgumentDefaultsRawHelpFormatter,
                                      description='Filter vcf files through the Ingenuity Variant Analysis API.')
    main_args = parser.add_argument_group("core arguments")
    main_args.add('vcf_files', type=vcf_file, nargs='+',
               help='vcf or vcf.gz files to be filtered by Ingenuity.')
    main_args.add('-u', '--user', required=True, type=email,
               help='e-mail of the Ingenuity user who is doing the analysis.')
    main_args.add('-a', '--analysis', required=True,
               help='custom analysis name (can be seen from Ingenuity Website)')
    main_args.add('-o', '--output_dir', type=writeable_directory, default='.',
               help='output path where the filtered vcf files will be stored. If empty they are just stored on current '
                    'path.')
    main_args.add('--suffix', default='_ing',
               help='suffix appended to the name of the output vcf file')
    conf_args = parser.add_argument_group("configuration arguments")
    conf_args.add('-c', '--config', default=os.path.join(script_path, '..', '.conf'), is_config_file=True,
                  help='config file path')
    conf_args.add('--client_id', required=True,
                  help='client_id is given by Ingenuity.')
    conf_args.add('--client_secret', required=True,
                  help='client_secret is given by Ingenuity.')
    conf_args.add('--temp_dir', default=os.path.join(script_path, '..', 'tmp'), type=writeable_directory,
                  help='directory for temporary files.')

    args = parser.parse_args()
    token = get_token()
    internet_connection = subprocess.Popen(
        """ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && echo ok || echo error""",
        shell=True, stdout=subprocess.PIPE).communicate()[0]
    if internet_connection == 'error':
        print('No internet access')
    elif token == 'invalid client id/client secret':
        print(token)
    else:
        for vcf in args.vcf_files:

            tmp = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
            tmp_path = os.path.join(script_path, args.temp_dir, tmp)
            os.makedirs(tmp_path)
            size_vcf = int(os.path.getsize(vcf))

            write_files_for_upload(tmp_path, vcf, args.user, args.analysis)

            print('Uploading ' + os.path.basename(vcf))
            http_status, iva_status, upload_message = make_request(upload_vcf, tmp_path, vcf)
            subprocess.check_call(['rm', tmp_path, '-r'])
            if http_status >= 400 or iva_status == 'FAILED':
                continue

            datapackage_url = json.loads(upload_message).get('status-url')

            print('Waiting for analysis to process', end='', flush=True)
            time.sleep(60)
            count = 1
            http_status, iva_status, _ = make_request(check_analysis_iva_status, datapackage_url, vcf)
            while http_status < 400 and iva_status != 'DONE' and iva_status != 'FAILED' and count < size_vcf/1000/30*3:
                print('.', end='')
                time.sleep(30)
                count += 1
                http_status, iva_status, _ = make_request(check_analysis_iva_status, datapackage_url, vcf)

            if http_status < 400 and iva_status != 'DONE'and iva_status != 'FAILED':
                time.sleep(60 * 15)
                http_status, iva_status, _ = make_request(check_analysis_iva_status, datapackage_url, vcf)
            if http_status >= 400 or iva_status == 'FAILED':
                continue

            print('\nExporting vcf to ' + os.path.join(os.path.abspath(args.output_dir),
                                                       os.path.basename(vcf).rstrip('.gz').rstrip('.vcf') +
                                                       args.suffix + '.vcf'))
            http_status, iva_status, export_message = make_request(export_vcf, datapackage_url, vcf, True)
            if http_status >= 400 or iva_status == 'FAILED':
                continue

            with open(os.path.join(args.output_dir,
                                   os.path.basename(vcf).rstrip('.gz').rstrip('.vcf') + args.suffix + '.vcf'), 'w') as f:
                f.write(export_message)
